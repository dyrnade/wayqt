/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/XdgPopup.hpp>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"

using namespace WQt;

/**
 * LayerShell Implementation
 */

LayerShell::LayerShell( zwlr_layer_shell_v1 *lShell, uint version ) {
    /* This is fully developed in wl_registry's hanbdle_global */
    mObj     = lShell;
    mVersion = version;
}


LayerShell::~LayerShell() {
    zwlr_layer_shell_v1_destroy( mObj );
}


LayerSurface *LayerShell::getLayerSurface( QWindow *window, wl_output *output, LayerShell::LayerType layer, const QString& lyrNs ) {
    wl_surface *surface = WQt::Utils::wlSurfaceFromQWindow( window );

    /* We need a valid wl_surface */
    if ( not surface ) {
        return nullptr;
    }

    /* We don't care if it's nullptr. Our compositor wll take case of this */
    if ( not output ) {
        output = WQt::Utils::wlOutputFromQScreen( window->screen() );
    }

    zwlr_layer_surface_v1 *lyr_surf = zwlr_layer_shell_v1_get_layer_surface( mObj, surface, output, layer, lyrNs.toUtf8().constData() );

    LayerSurface *lyrSurface = new LayerSurface( window, lyr_surf, mVersion );

    return lyrSurface;
}


zwlr_layer_shell_v1 *LayerShell::get() {
    return mObj;
}


/**
 * LayerSurface Implementation
 */
void LayerSurface::configureCallback( void *data, struct ::zwlr_layer_surface_v1 *, uint32_t serial, uint32_t width, uint32_t height ) {
    auto ls = reinterpret_cast<LayerSurface *>(data);

    ls->configureSurface( serial, width, height );
}


void LayerSurface::closedCallback( void *data, struct ::zwlr_layer_surface_v1 * ) {
    auto ls = reinterpret_cast<LayerSurface *>(data);

    ls->closeSurface();
}


const struct zwlr_layer_surface_v1_listener WQt::LayerSurface::mLyrSurfListener = {
    configureCallback,
    closedCallback,
};

LayerSurface::LayerSurface( QWindow *window, zwlr_layer_surface_v1 *surf, uint version ) {
    mWindow  = window;
    mObj     = surf;
    mVersion = version;

    zwlr_layer_surface_v1_add_listener( surf, &mLyrSurfListener, this );
    wl_display_roundtrip( WQt::Wayland::display() );
}


LayerSurface::~LayerSurface() {
    zwlr_layer_surface_v1_destroy( mObj );
}


void LayerSurface::apply() {
    zwlr_layer_surface_v1_set_anchor( mObj, static_cast<uint32_t>(m_anchors) );
    zwlr_layer_surface_v1_set_exclusive_zone( mObj, m_exclusiveZone );
    zwlr_layer_surface_v1_set_keyboard_interactivity( mObj, m_keyboardInteractivity );

    if ( m_surfaceSize.isValid() ) {
        zwlr_layer_surface_v1_set_size( mObj, m_surfaceSize.width(), m_surfaceSize.height() );
    }

    else {
        zwlr_layer_surface_v1_set_size( mObj, mWindow->width(), mWindow->height() );
    }

    zwlr_layer_surface_v1_set_margin( mObj, m_margins.top(), m_margins.right(), m_margins.bottom(), m_margins.left() );

    wl_surface_commit( WQt::Utils::wlSurfaceFromQWindow( mWindow ) );
    wl_display_roundtrip( WQt::Wayland::display() );
}


void LayerSurface::setSurfaceSize( const QSize& surfaceSize ) {
    m_surfaceSize = surfaceSize;
    zwlr_layer_surface_v1_set_size( mObj, m_surfaceSize.width(), m_surfaceSize.height() );
    wl_surface_commit( WQt::Utils::wlSurfaceFromQWindow( mWindow ) );
}


void LayerSurface::setAnchors( const SurfaceAnchors& anchors ) {
    m_anchors = anchors;
    zwlr_layer_surface_v1_set_anchor( mObj, (uint32_t)m_anchors );
}


void LayerSurface::setExclusiveZone( int exclusiveZone ) {
    m_exclusiveZone = exclusiveZone;
    zwlr_layer_surface_v1_set_exclusive_zone( mObj, m_exclusiveZone );
}


void LayerSurface::setMargins( const QMargins& margins ) {
    m_margins = margins;
    zwlr_layer_surface_v1_set_margin( mObj, m_margins.top(), m_margins.right(), m_margins.bottom(), m_margins.left() );
}


void LayerSurface::setKeyboardInteractivity( LayerSurface::FocusType focusType ) {
    m_keyboardInteractivity = ( uint )focusType;

    /**
     * Protocol version 3
     * Protocol version 3 does not have support for NoFocus/Exclusive/OnDemand enums.
     * It gives us only two choices: 0 and 1.
     */
    if ( mVersion == 3 ) {
        zwlr_layer_surface_v1_set_keyboard_interactivity( mObj, (m_keyboardInteractivity > 0 ? 1 : 0) );
    }

    /**
     * Protocol version 4 or higher
     * Protocol version 4 introduced support for NoFocus/Exclusive/OnDemand enums.
     */
    else {
        zwlr_layer_surface_v1_set_keyboard_interactivity( mObj, m_keyboardInteractivity );
    }
}


void LayerSurface::setLayer( WQt::LayerShell::LayerType type ) {
    m_lyrType = type;
    zwlr_layer_surface_v1_set_layer( mObj, ( uint )m_lyrType );
}


void LayerSurface::getPopup( WQt::XdgPopup *popup ) {
    zwlr_layer_surface_v1_get_popup( mObj, popup->get() );
}


zwlr_layer_surface_v1 *LayerSurface::get() {
    return mObj;
}


void LayerSurface::configureSurface( uint32_t serial, uint32_t width, uint32_t height ) {
    mWindow->resize( width, height );
    zwlr_layer_surface_v1_ack_configure( mObj, serial );
}


void LayerSurface::closeSurface() {
    mWindow->close();
}
