/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <png.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QGuiApplication>
#include <wayland-client.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/ScreenCopy.hpp>

#include "wlr-screencopy-unstable-v1-client-protocol.h"


WQt::ScreenCopyManager::ScreenCopyManager( zwlr_screencopy_manager_v1 *mgr ) {
    mObj = mgr;
}


WQt::ScreenCopyManager::~ScreenCopyManager() {
    zwlr_screencopy_manager_v1_destroy( mObj );
}


WQt::ScreenCopyFrame *WQt::ScreenCopyManager::captureOutput( bool drawCursor, struct wl_output *output ) {
    struct zwlr_screencopy_frame_v1 *frame = zwlr_screencopy_manager_v1_capture_output( mObj, (drawCursor ? 1 : 0), output );

    return new ScreenCopyFrame( frame );
}


WQt::ScreenCopyFrame *WQt::ScreenCopyManager::captureOutputRegion( bool drawCursor, struct wl_output *output, QRect rect ) {
    struct zwlr_screencopy_frame_v1 *frame = zwlr_screencopy_manager_v1_capture_output_region(
        mObj, (drawCursor ? 1 : 0), output, rect.x(), rect.y(), rect.width(), rect.height()
    );

    return new ScreenCopyFrame( frame );
}


zwlr_screencopy_manager_v1 *WQt::ScreenCopyManager::get() {
    return mObj;
}


WQt::ScreenCopyFrame::ScreenCopyFrame( zwlr_screencopy_frame_v1 *frame ) {
    mObj = frame;
}


WQt::ScreenCopyFrame::~ScreenCopyFrame() {
    zwlr_screencopy_frame_v1_destroy( mObj );
}


void WQt::ScreenCopyFrame::setup() {
    zwlr_screencopy_frame_v1_add_listener( mObj, &mListener, this );
}


bool WQt::ScreenCopyFrame::saveAsImage( QString filename ) {
    for ( int i = 0; i < mReceivedBuffers.count(); i++ ) {
        if ( mFormats.keys().contains( mReceivedBuffers[ i ].format ) ) {
            mBuffer.info      = mReceivedBuffers[ i ];
            mBuffer.wl_buffer = createShmBuffer( mBuffer.info, &mBuffer.data );

            if ( mBuffer.wl_buffer == NULL ) {
                qWarning() << "Failed to create buffer with format" << mBuffer.info.format;
                continue;
            }

            zwlr_screencopy_frame_v1_copy( mObj, mBuffer.wl_buffer );
            break;
        }
    }

    /** Wait until copy is successful, or it fails. */
    while ( true ) {
        /** If screencopy failed, nothing to write */
        if ( mFailed ) {
            qCritical() << "zwlr_screencopy_frame_v1_copy() failed";
            return false;
        }

        if ( mCopyDone ) {
            break;
        }

        qApp->processEvents();
    }

    QImage img( (uchar *)mBuffer.data, mBuffer.info.width, mBuffer.info.height, mBuffer.info.stride, QImage::Format_ARGB32 );

    return img.save( filename );
}


zwlr_screencopy_frame_v1 *WQt::ScreenCopyFrame::get() {
    return mObj;
}


void WQt::ScreenCopyFrame::handleBuffer( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t fmt, uint32_t w, uint32_t h, uint32_t stride ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    FrameBufferInfo info       = { (wl_shm_format)fmt, w, h, stride };

    scrnFrame->mReceivedBuffers << info;
}


void WQt::ScreenCopyFrame::handleFlags( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t flags ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);

    scrnFrame->mYInvert = flags & ZWLR_SCREENCOPY_FRAME_V1_FLAGS_Y_INVERT;
}


void WQt::ScreenCopyFrame::handleReady( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    emit scrnFrame->ready();

    scrnFrame->mCopyDone = true;
}


void WQt::ScreenCopyFrame::handleFailed( void *data, struct zwlr_screencopy_frame_v1 * ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    emit scrnFrame->failed();

    scrnFrame->mFailed = true;
}


void WQt::ScreenCopyFrame::handleDamage( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t, uint32_t ) {
}


void WQt::ScreenCopyFrame::handleLinuxDmabuf( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t ) {
}


void WQt::ScreenCopyFrame::handleBufferDone( void *data, struct zwlr_screencopy_frame_v1 * ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    emit scrnFrame->bufferDone();
}


struct wl_buffer *WQt::ScreenCopyFrame::createShmBuffer( FrameBufferInfo info, void **data_out ) {
    int size = info.stride * info.height;

    const char shm_name[] = "/wlroots-screencopy";
    int        fd         = shm_open( shm_name, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR );

    if ( fd < 0 ) {
        qCritical() << "shm_open() failed";
        return nullptr;
    }

    shm_unlink( shm_name );

    int ret;

    while ( (ret = ftruncate( fd, size ) ) == EINTR ) {
        // No-op
    }

    if ( ret < 0 ) {
        close( fd );
        qCritical() << "ftruncate() failed";
        return nullptr;
    }

    void *data = mmap( NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );

    if ( data == MAP_FAILED ) {
        qCritical() << "mmap() failed";
        close( fd );
        return nullptr;
    }

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    struct wl_shm *shm = reg->waylandShm();

    if ( shm == nullptr ) {
        qCritical() << "No allocated shared memory";
        return nullptr;
    }

    struct wl_shm_pool *pool = wl_shm_create_pool( shm, fd, size );

    close( fd );
    struct wl_buffer *buffer = wl_shm_pool_create_buffer( pool, 0, info.width, info.height, info.stride, info.format );

    wl_shm_pool_destroy( pool );

    *data_out = data;
    return buffer;
}


const zwlr_screencopy_frame_v1_listener WQt::ScreenCopyFrame::mListener = {
    handleBuffer,
    handleFlags,
    handleReady,
    handleFailed,
    handleDamage,
    handleLinuxDmabuf,
    handleBufferDone,
};


/**
 * STEPS:
 * 1. SCM requests to capture the output.
 * 2. We get SCF object.
 * 3. SCF emits a series of signals - one for each buffer format.
 * 4. Once all formats are received, bufferDone is emitted.
 * 5. Now, we can try to write the image.
 *    a. Search if we have a suitable format.
 *    b. Create the buffer for that format with suitable size, and stride
 *    c. If buffer was created, then perform the copy()
 *    d. If we were successful, save the image as PNG using the given filename.
 **/
