/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>

#include <wayqt/Idle.hpp>
#include <wayqt/Output.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/XdgShell.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/ScreenCopy.hpp>
#include <wayqt/SessionLock.hpp>
#include <wayqt/DataControl.hpp>
#include <wayqt/GammaControl.hpp>
#include <wayqt/WayfireShell.hpp>
#include <wayqt/OutputManager.hpp>
#include <wayqt/WindowManager.hpp>
#include <wayqt/InputInhibition.hpp>
#include <wayqt/OutputPowerManager.hpp>

#include "idle-client-protocol.h"
#include "wayland-client-protocol.h"
#include "xdg-shell-client-protocol.h"
#include "ext-session-lock-v1-client-protocol.h"
#include "wayfire-shell-unstable-v2-client-protocol.h"
#include "wlr-screencopy-unstable-v1-client-protocol.h"
#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "wlr-data-control-unstable-v1-client-protocol.h"
#include "wlr-gamma-control-unstable-v1-client-protocol.h"
#include "wlr-input-inhibitor-unstable-v1-client-protocol.h"
#include "wlr-output-management-unstable-v1-client-protocol.h"
#include "wlr-output-power-management-unstable-v1-client-protocol.h"
#include "wlr-foreign-toplevel-management-unstable-v1-client-protocol.h"

/* Convenience functions */
void WQt::Registry::globalAnnounce( void *data, struct wl_registry *, uint32_t name, const char *interface, uint32_t version ) {
    auto r = reinterpret_cast<WQt::Registry *>(data);

    r->handleAnnounce( name, interface, version );
}


void WQt::Registry::globalRemove( void *data, struct wl_registry *, uint32_t name ) {
    // who cares :D
    // but we will call WQt::Registry::handleRemove just for the heck of it

    auto r = reinterpret_cast<WQt::Registry *>(data);

    r->handleRemove( name );
}


const struct wl_registry_listener WQt::Registry::mRegListener = {
    globalAnnounce,
    globalRemove,
};

WQt::Registry::Registry( wl_display *wlDisplay ) {
    mWlDisplay = wlDisplay;
    mObj       = wl_display_get_registry( mWlDisplay );
}


WQt::Registry::~Registry() {
    wl_registry_destroy( mObj );
}


void WQt::Registry::setup() {
    wl_registry_add_listener( mObj, &mRegListener, this );
    wl_display_roundtrip( mWlDisplay );
}


WQt::Registry::operator wl_registry *() const {
    return mObj;
}


WQt::Registry::operator wl_registry *() {
    return mObj;
}


wl_display *WQt::Registry::waylandDisplay() {
    return mWlDisplay;
}


wl_seat *WQt::Registry::waylandSeat() {
    return mWlSeat;
}


wl_shm *WQt::Registry::waylandShm() {
    return mWlShm;
}


QList<WQt::Output *> WQt::Registry::waylandOutputs() {
    return mOutputs.values();
}


WQt::XdgShell *WQt::Registry::xdgShell() {
    return mXdgShell;
}


WQt::LayerShell *WQt::Registry::layerShell() {
    return mLayerShell;
}


WQt::Shell *WQt::Registry::wayfireShell() {
    return mWayfireShell;
}


WQt::WindowManager *WQt::Registry::windowManager() {
    return mWindowMgr;
}


WQt::InputInhibitManager *WQt::Registry::inputInhibitManager() {
    return mInhibitManager;
}


WQt::ScreenCopyManager *WQt::Registry::screenCopier() {
    return mScreenCopyMgr;
}


WQt::DataControlManager *WQt::Registry::dataControlManager() {
    return mDataControlMgr;
}


WQt::OutputPowerManager *WQt::Registry::outputPowerManager() {
    return mOutputPowerMgr;
}


WQt::OutputManager *WQt::Registry::outputManager() {
    return mOutputMgr;
}


WQt::SessionLockManager *WQt::Registry::sessionLockManager() {
    return mSessLockMgr;
}


WQt::IdleManager *WQt::Registry::idleManager() {
    return mIdleMgr;
}


WQt::GammaControlManager *WQt::Registry::gammaControlManager() {
    return mGammaCtrl;
}


void WQt::Registry::handleAnnounce( uint32_t name, const char *interface, uint32_t version ) {
    /**
     * We really don't care about wl_seat version right now.
     */
    if ( strcmp( interface, wl_seat_interface.name ) == 0 ) {
        mWlSeat = (wl_seat *)wl_registry_bind( mObj, name, &wl_seat_interface, version );

        if ( !mWlSeat ) {
            emit errorOccured( WQt::Registry::EmptySeat );
        }
    }

    /**
     * We really don't care about wl_shm version right now.
     */
    if ( strcmp( interface, wl_shm_interface.name ) == 0 ) {
        mWlShm = (wl_shm *)wl_registry_bind( mObj, name, &wl_shm_interface, version );

        if ( !mWlShm ) {
            emit errorOccured( WQt::Registry::EmptyShm );
        }
    }

    /**
     * We really don't care about wl_output version right now.
     */
    if ( strcmp( interface, wl_output_interface.name ) == 0 ) {
        wl_output *op = (wl_output *)wl_registry_bind( mObj, name, &wl_output_interface, version );

        if ( op ) {
            mOutputs[ name ] = new WQt::Output( op );
            emit outputAdded( mOutputs[ name ] );
        }
    }

    /**
     * We really don't care about xdg_wm_base version right now.
     */
    else if ( strcmp( interface, xdg_wm_base_interface.name ) == 0 ) {
        mXdgWmBase = (xdg_wm_base *)wl_registry_bind( mObj, name, &xdg_wm_base_interface, version );

        if ( !mXdgWmBase ) {
            emit errorOccured( WQt::Registry::EmptyXdgWmBase );
        }

        else {
            mXdgShell = new WQt::XdgShell( mXdgWmBase );
        }
    }

    /**
     * Wlroots 0.13.0 introduced support for yes/no/maybe.
     */
    else if ( strcmp( interface, zwlr_layer_shell_v1_interface.name ) == 0 ) {
        mWlrLayerShell = (zwlr_layer_shell_v1 *)wl_registry_bind( mObj, name, &zwlr_layer_shell_v1_interface, version );

        if ( !mWlrLayerShell ) {
            emit errorOccured( WQt::Registry::EmptyLayerShell );
        }

        else {
            mLayerShell = new WQt::LayerShell( mWlrLayerShell, version );
        }
    }

    /**
     * We're happy with what's available in version 1.
     */
    else if ( strcmp( interface, zwlr_input_inhibit_manager_v1_interface.name ) == 0 ) {
        mWlrInhibitMgr = (zwlr_input_inhibit_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_input_inhibit_manager_v1_interface, 1 );

        if ( !mWlrInhibitMgr ) {
            emit errorOccured( WQt::Registry::EmptyInputInhibitManager );
        }

        else {
            mInhibitManager = new WQt::InputInhibitManager( mWlrInhibitMgr );
        }
    }

    /**
     * We're happy with what's available in version 3.
     */
    else if ( strcmp( interface, zwlr_foreign_toplevel_manager_v1_interface.name ) == 0 ) {
        mWlrWindowMgr = (zwlr_foreign_toplevel_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_foreign_toplevel_manager_v1_interface, 3 );

        if ( !mWlrWindowMgr ) {
            emit errorOccured( WQt::Registry::EmptyToplevelManager );
        }

        else {
            mWindowMgr = new WQt::WindowManager( mWlrWindowMgr );
        }
    }

    /**
     * We're happy with what's available in version 1.
     */
    else if ( strcmp( interface, zwf_shell_manager_v2_interface.name ) == 0 ) {
        mWfShellMgr = (zwf_shell_manager_v2 *)wl_registry_bind( mObj, name, &zwf_shell_manager_v2_interface, 1 );

        if ( !mWfShellMgr ) {
            emit errorOccured( WQt::Registry::EmptyWayfireShell );
        }

        else {
            mWayfireShell = new WQt::Shell( mWfShellMgr );
        }
    }

    /**
     * We're happy with what's available in version 1.
     */
    else if ( strcmp( interface, org_kde_kwin_idle_interface.name ) == 0 ) {
        mKdeIdle = (org_kde_kwin_idle *)wl_registry_bind( mObj, name, &org_kde_kwin_idle_interface, 1 );

        if ( !mKdeIdle ) {
            emit errorOccured( WQt::Registry::EmptyIdle );
        }

        else {
            mIdleMgr = new WQt::IdleManager( mKdeIdle );
        }
    }

    /**
     * We've implemented version 3.
     * And wlroots 0.15.0 has version 3 available.
     */
    else if ( strcmp( interface, zwlr_screencopy_manager_v1_interface.name ) == 0 ) {
        mWlrScreenCopyMgr = (zwlr_screencopy_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_screencopy_manager_v1_interface, 3 );

        if ( !mWlrScreenCopyMgr ) {
            emit errorOccured( WQt::Registry::EmptyScreenCopyManager );
        }

        else {
            mScreenCopyMgr = new WQt::ScreenCopyManager( mWlrScreenCopyMgr );
        }
    }

    /**
     * We've implemented version 2.
     * And wlroots 0.15.0 has version 2 available.
     */
    else if ( strcmp( interface, zwlr_data_control_manager_v1_interface.name ) == 0 ) {
        mWlrDataControlMgr = (zwlr_data_control_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_data_control_manager_v1_interface, 2 );

        if ( !mWlrDataControlMgr ) {
            emit errorOccured( WQt::Registry::EmptyDataControlManager );
        }

        else {
            mDataControlMgr = new WQt::DataControlManager( mWlrDataControlMgr );
        }
    }

    /**
     * We've implemented version 1.
     * And wlroots 0.15.0 has version 1 available.
     */
    else if ( strcmp( interface, zwlr_output_power_manager_v1_interface.name ) == 0 ) {
        mWlrOutputPowerMgr = (zwlr_output_power_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_output_power_manager_v1_interface, 1 );

        if ( !mWlrOutputPowerMgr ) {
            emit errorOccured( WQt::Registry::EmptyOutputPowerManager );
        }

        else {
            mOutputPowerMgr = new WQt::OutputPowerManager( mWlrOutputPowerMgr );
        }
    }

    /**
     * We've implemented version 2.
     * And wlroots 0.15.0 has version 2 available.
     */
    else if ( strcmp( interface, zwlr_output_manager_v1_interface.name ) == 0 ) {
        mWlrOutputMgr = (zwlr_output_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_output_manager_v1_interface, 2 );

        if ( !mWlrOutputMgr ) {
            emit errorOccured( WQt::Registry::EmptyOutputManager );
        }

        else {
            mOutputMgr = new WQt::OutputManager( mWlrOutputMgr );
        }
    }

    /**
     * We've implemented version 1.
     * And wlroots 0.15.0 has version 1 available.
     */
    else if ( strcmp( interface, ext_session_lock_manager_v1_interface.name ) == 0 ) {
        mExtSessLockMgr = (ext_session_lock_manager_v1 *)wl_registry_bind( mObj, name, &ext_session_lock_manager_v1_interface, 1 );

        if ( !mExtSessLockMgr ) {
            emit errorOccured( WQt::Registry::EmptySessionLockManager );
        }

        else {
            mSessLockMgr = new WQt::SessionLockManager( mExtSessLockMgr );
        }
    }

    /**
     * We've implemented version 1.
     * And wlroots 0.15.0 has version 1 available.
     */
    else if ( strcmp( interface, zwlr_gamma_control_manager_v1_interface.name ) == 0 ) {
        mWlrGammaCtrl = (zwlr_gamma_control_manager_v1 *)wl_registry_bind( mObj, name, &zwlr_gamma_control_manager_v1_interface, 1 );

        if ( !mWlrGammaCtrl ) {
            emit errorOccured( WQt::Registry::EmptyGammaControlManager );
        }

        else {
            mGammaCtrl = new WQt::GammaControlManager( mWlrGammaCtrl );
        }
    }
}


void WQt::Registry::handleRemove( uint32_t name ) {
    /**
     * While we do not care about most of the handleRemove,
     * we need to worry about the wl_output * objects.
     */
    if ( mOutputs.keys().contains( name ) ) {
        WQt::Output *output = mOutputs.take( name );
        emit        outputRemoved( output );
    }
}
