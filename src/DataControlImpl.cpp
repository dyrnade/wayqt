/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QCoreApplication>
#include <QObject>
#include <QThread>
#include <QFile>
#include <QDebug>

#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>

#include "DataControlImpl.hpp"

/**
 * Timed IO
 * Sometimes, GTK apps cause problems while reading the data
 * they provide. In such cases, we should try to read data for
 * upto 1s, and then, quit. This QObject-derived class does
 * exactly what's written above.
 * Currently, we read the data in 4 batches of 250ms wait.
 */
PipeReader::PipeReader() : QThread() {
    /**
     * In the current implementation,
     * we don't have any thing to initialize.
     * This behaviour can change at a later stage.
     */
}


void PipeReader::readFromPipe( int fd ) {
    mFD   = fd;
    mData = QByteArray();

    if ( not isRunning() ) {
        start();
    }
}


QByteArray PipeReader::data() const {
    return mData;
}


void PipeReader::run() {
    QFile readPipe;

    if ( readPipe.open( mFD, QIODevice::ReadOnly, QFile::AutoCloseHandle ) ) {
        readData();
        close( mFD );
    }
}


void PipeReader::readData() {
    pollfd pfds[ 1 ];

    pfds[ 0 ].fd     = mFD;
    pfds[ 0 ].events = POLLIN;

    while ( true ) {
        const int ready = poll( pfds, 1, 1000 );

        if ( ready < 0 ) {
            if ( errno != EINTR ) {
                qWarning( "DataControlOffer: poll() failed: %s", strerror( errno ) );
                return;
            }
        }
        else if ( ready == 0 ) {
            qWarning( "DataControlOffer: timeout reading from pipe" );
            return;
        }
        else {
            char buf[ 4096 ];
            int  n = read( mFD, buf, sizeof buf );

            if ( n < 0 ) {
                qWarning( "DataControlOffer: read() failed: %s", strerror( errno ) );
                return;
            }
            else if ( n == 0 ) {
                return;
            }
            else if ( n > 0 ) {
                mData.append( buf, n );
            }
        }
    }
}


PipeWriter::PipeWriter() : QThread() {
    /**
     * In the current implementation,
     * we don't have any thing to initialize.
     * This behaviour can change at a later stage.
     */
}


void PipeWriter::writeData( int fd, const QByteArray& data ) {
    mFD   = fd;
    mData = data;

    if ( not isRunning() ) {
        start();
    }
}


void PipeWriter::run() {
    QFile c;

    if ( c.open( mFD, QFile::WriteOnly, QFile::AutoCloseHandle ) ) {
        /**
         * Create a sigpipe handler that does nothing,
         * or clients may be forced to terminate
         * if the pipe is closed in the other end.
         */
        struct sigaction action, oldAction;
        action.sa_handler = SIG_IGN;
        sigemptyset( &action.sa_mask );
        action.sa_flags = 0;

        /**
         * Set a new action for the signal SIGPIPE: Ignore.
         * Store the old action in &oldAction.
         */
        sigaction( SIGPIPE, &action, &oldAction );

        /** Unset O_NONBLOCK */
        fcntl( mFD, F_SETFL, 0 );

        /** Write data */
        const qint64 written = c.write( mData );

        /**
         * Restore the old action.
         */
        sigaction( SIGPIPE, &oldAction, nullptr );

        /** Close the file */
        c.close();

        if ( written != mData.size() ) {
            qWarning() << "Failed to send all clipobard data; sent"
                       << written << "bytes out of" << mData.size();
        }
    }
}
