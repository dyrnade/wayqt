/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include "wayqt/InputInhibition.hpp"

#include "wlr-input-inhibitor-unstable-v1-client-protocol.h"

/**
 * WQt::InputInhibitManager
 */

WQt::InputInhibitManager::InputInhibitManager( zwlr_input_inhibit_manager_v1 *iimgr ) : QObject() {
    mObj = iimgr;
}


WQt::InputInhibitManager::~InputInhibitManager() {
    zwlr_input_inhibit_manager_v1_destroy( mObj );
}


WQt::InputInhibitor *WQt::InputInhibitManager::getInputInhibitor() {
    if ( not mObj ) {
        return nullptr;
    }

    return new WQt::InputInhibitor( zwlr_input_inhibit_manager_v1_get_inhibitor( mObj ) );
}


zwlr_input_inhibit_manager_v1 *WQt::InputInhibitManager::get() {
    return mObj;
}


/**
 * WQt::InputInhibitor
 */

WQt::InputInhibitor::InputInhibitor( zwlr_input_inhibitor_v1 *ii ) : QObject() {
    mObj = ii;
}


WQt::InputInhibitor::~InputInhibitor() {
    zwlr_input_inhibitor_v1_destroy( mObj );
}


bool WQt::InputInhibitor::isValid() {
    return (mObj == nullptr);
}


zwlr_input_inhibitor_v1 *WQt::InputInhibitor::get() {
    return mObj;
}
