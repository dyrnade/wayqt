/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtWidgets>

#include <wayland-client.h>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/ScreenCopy.hpp>


int main( int argc, char *argv[] ) {
    QApplication *app = new QApplication( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    WQt::ScreenCopyManager *scm   = reg->screenCopier();
    WQt::ScreenCopyFrame   *frame = scm->captureOutput( false, WQt::getWlOutput( app->primaryScreen() ) );

    QObject::connect(
        frame, &WQt::ScreenCopyFrame::bufferDone, [ = ] () {
            if ( frame->saveAsImage( ( (argc == 1)? "image.png" : argv[ 1 ]) ) ) {
                qDebug() << "Screenshot successful";
            }
            else {
                qDebug() << "Screenshot failed";
            }

            app->quit();
        }
    );

    frame->setup();

    return app->exec();
}
