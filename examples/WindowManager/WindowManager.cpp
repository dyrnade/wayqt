/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtWidgets>

#include <wayland-client.h>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/WindowManager.hpp>

int main( int argc, char *argv[] ) {
    QApplication *app = new QApplication( argc, argv );

    app->setDesktopFileName( "wayqt-test.desktop" );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    WQt::WindowManager *wm = reg->windowManager();

    wm->setup();

    QHash<WQt::WindowHandle *, bool>    map;
    QHash<WQt::WindowHandle *, QString> titleMap;
    QHash<WQt::WindowHandle *, QString> appIdMap;

    QObject::connect(
        wm, &WQt::WindowManager::newTopLevelHandle, [ = ] ( WQt::WindowHandle *handle ) mutable {
            map[ handle ]      = false;
            titleMap[ handle ] = QString();
            appIdMap[ handle ] = QString();

            QObject::connect(
                handle, &WQt::WindowHandle::stateChanged, [ = ] () mutable {
                    bool minimized  = handle->isMinimized();
                    bool maximized  = handle->isMaximized();
                    bool activated  = handle->isActivated();
                    bool fullscreen = handle->isFullScreen();

                    qInfo() << handle->title() << "--" << handle->appId();
                    qInfo() << "    Maximized:  " << maximized;
                    qInfo() << "    Minimized:  " << minimized;
                    qInfo() << "    Activated:  " << activated;
                    qInfo() << "    FullScreen: " << fullscreen << "\n";
                }
            );

            handle->setup();
        }
    );

    QMainWindow *mw  = new QMainWindow();
    QPushButton *btn = new QPushButton( QIcon::fromTheme( "application-exit" ), "Close App", mw );

    mw->setCentralWidget( btn );
    mw->show();

    QObject::connect( btn, &QPushButton::clicked, app, &QApplication::quit );

    return app->exec();
}
