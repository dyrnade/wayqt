/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QMargins>
#include <QSize>

class QWindow;

struct zwlr_layer_shell_v1;
struct zwlr_layer_surface_v1;
struct zwlr_layer_surface_v1_listener;
struct wl_output;

namespace WQt {
    class XdgPopup;
}

namespace WQt {
    class LayerShell;
    class LayerSurface;
}

class WQt::LayerShell : public QObject {
    Q_OBJECT;

    public:
        enum LayerType {
            Background = 0,
            Bottom     = 1,
            Top        = 2,
            Overlay    = 3
        };

        LayerShell( zwlr_layer_shell_v1 *lShell, uint version );
        ~LayerShell();

        LayerSurface *getLayerSurface( QWindow *window, wl_output *output, LayerType layer, const QString& lyrNs );

        zwlr_layer_shell_v1 *get();

    private:
        zwlr_layer_shell_v1 *mObj;
        uint mVersion;
};

class WQt::LayerSurface : public QObject {
    Q_OBJECT;

    public:
        enum SurfaceAnchor {
            NoAchor = 0,
            Top     = (1 << 0),
            Bottom  = (1 << 1),
            Left    = (1 << 2),
            Right   = (1 << 3)
        };
        Q_DECLARE_FLAGS( SurfaceAnchors, SurfaceAnchor );

        /* Keyboard focus */
        enum FocusType {
            NoFocus   = 0,
            Exclusive = 1,
            OnDemand  = 2
        };

        LayerSurface( QWindow *window, zwlr_layer_surface_v1 *lyrSurf, uint version );
        ~LayerSurface();

        void apply();

        void setSurfaceSize( const QSize& surfaceSize );
        void setAnchors( const SurfaceAnchors& anchors );
        void setExclusiveZone( int exclusiveZone );
        void setMargins( const QMargins& margins );
        void setKeyboardInteractivity( FocusType focusType );
        void setLayer( WQt::LayerShell::LayerType type );

        void getPopup( WQt::XdgPopup *popup );

        zwlr_layer_surface_v1 *get();

    private:
        uint mVersion;
        QWindow *mWindow;
        zwlr_layer_surface_v1 *mObj;

        QSize m_surfaceSize{ 0, 0 };
        int m_exclusiveZone = -1;
        QMargins m_margins{ 0, 0, 0, 0 };
        uint m_keyboardInteractivity = 1;

        SurfaceAnchors m_anchors;
        WQt::LayerShell::LayerType m_lyrType;

        void configureSurface( uint32_t serial, uint32_t width, uint32_t height );
        void closeSurface();

        static void configureCallback( void *data, zwlr_layer_surface_v1 *object, uint32_t serial, uint32_t width, uint32_t height );
        static void closedCallback( void *data, zwlr_layer_surface_v1 *object );

        static const zwlr_layer_surface_v1_listener mLyrSurfListener;
};

Q_DECLARE_OPERATORS_FOR_FLAGS( WQt::LayerSurface::SurfaceAnchors );
