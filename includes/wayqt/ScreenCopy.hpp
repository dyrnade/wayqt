/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QMap>
#include <QRect>
#include <QObject>
#include <QString>
#include <wayland-client-protocol.h>


struct wl_buffer;
struct wl_output;
struct wl_shm;
struct zwlr_screencopy_frame_v1;
struct zwlr_screencopy_frame_v1_listener;
struct zwlr_screencopy_manager_v1;

namespace WQt {
    class ScreenCopyManager;
    class ScreenCopyFrame;

    typedef struct _bufferInfo {
        enum wl_shm_format format;
        uint32_t           width;
        uint32_t           height;
        uint32_t           stride;
    } FrameBufferInfo;

    typedef struct _buffer {
        struct wl_buffer *wl_buffer;
        void             *data;
        FrameBufferInfo  info;
    } FrameBuffer;
}

class WQt::ScreenCopyManager : public QObject {
    Q_OBJECT;

    public:
        ScreenCopyManager( zwlr_screencopy_manager_v1 *scrnMgr );
        ~ScreenCopyManager();

        ScreenCopyFrame *captureOutput( bool, struct wl_output * );
        ScreenCopyFrame *captureOutputRegion( bool, struct wl_output *, QRect );

        zwlr_screencopy_manager_v1 *get();

    private:
        zwlr_screencopy_manager_v1 *mObj;
};

class WQt::ScreenCopyFrame : public QObject {
    Q_OBJECT;

    public:
        enum Error {
            AlreadyUsed = 0,
            InvalidBuffer
        };

        ScreenCopyFrame( zwlr_screencopy_frame_v1 * );
        ~ScreenCopyFrame();

        /** Setup the listener. First connect the signals to your slots, and then call this */
        void setup();

        /** Call this once bufferDone() is emitted */
        bool saveAsImage( QString fileName );

        zwlr_screencopy_frame_v1 *get();

    private:
        static void handleBuffer( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t, uint32_t );
        static void handleFlags( void *, struct zwlr_screencopy_frame_v1 *, uint32_t );
        static void handleReady( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t );
        static void handleFailed( void *, struct zwlr_screencopy_frame_v1 * );
        static void handleDamage( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t, uint32_t );
        static void handleLinuxDmabuf( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t );
        static void handleBufferDone( void *, struct zwlr_screencopy_frame_v1 * );

        struct wl_buffer *createShmBuffer( FrameBufferInfo, void ** );

        static const zwlr_screencopy_frame_v1_listener mListener;

        zwlr_screencopy_frame_v1 *mObj;

        QMap<wl_shm_format, bool> mFormats{
            { WL_SHM_FORMAT_XRGB8888, true },
            { WL_SHM_FORMAT_ARGB8888, true },
            { WL_SHM_FORMAT_XBGR8888, false },
            { WL_SHM_FORMAT_ABGR8888, false },
        };

        QList<FrameBufferInfo> mReceivedBuffers;
        FrameBuffer mBuffer;

        bool mYInvert  = false;
        bool mCopyDone = false;
        bool mFailed   = false;

    Q_SIGNALS:
        void ready();
        void failed();
        void damage();
        void linuxDmabuf();
        void bufferDone();
};
