/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QHash>

struct wl_registry;
struct wl_display;
struct wl_seat;
struct wl_shm;
struct wl_output;
// struct wl_compositor;

struct wl_registry_listener;

struct xdg_wm_base;
struct org_kde_kwin_idle;
struct zwlr_layer_shell_v1;
struct zwf_shell_manager_v2;
struct zwlr_output_manager_v1;
struct zwlr_screencopy_manager_v1;
struct ext_session_lock_manager_v1;
struct zwlr_data_control_manager_v1;
struct zwlr_output_power_manager_v1;
struct zwlr_gamma_control_manager_v1;
struct zwlr_input_inhibit_manager_v1;
struct zwlr_foreign_toplevel_manager_v1;

namespace WQt {
    class Registry;
    class XdgShell;

    class Output;
    class LayerShell;
    class IdleManager;
    class LayerSurface;
    class OutputManager;
    class WindowManager;
    class ScreenCopyManager;
    class DataControlManager;
    class SessionLockManager;
    class OutputPowerManager;
    class GammaControlManager;
    class InputInhibitManager;

    class Shell;
}

class WQt::Registry : public QObject {
    Q_OBJECT;

    public:
        enum ErrorType {
            EmptyShm,
            EmptyIdle,
            EmptySeat,
            EmptyXdgWmBase,
            EmptyCompositor,
            EmptyLayerShell,
            EmptyWayfireShell,
            EmptyOutputManager,
            EmptyToplevelManager,
            EmptyScreenCopyManager,
            EmptyDataControlManager,
            EmptyOutputPowerManager,
            EmptySessionLockManager,
            EmptyGammaControlManager,
            EmptyInputInhibitManager,
        };
        Q_ENUM( ErrorType );

        Registry( wl_display *wlDisplay );
        ~Registry();

        void setup();

        operator wl_registry *();
        operator wl_registry *() const;

        wl_display *waylandDisplay();
        wl_seat *waylandSeat();
        wl_shm *waylandShm();

        QList<WQt::Output *> waylandOutputs();

        /* Ready to use Wayland Classes */

        /**
         * LayerShell - Wlr Layer Shell protocol implementation
         */
        WQt::XdgShell *xdgShell();

        /**
         * LayerShell - Wlr Layer Shell protocol implementation
         */
        WQt::LayerShell *layerShell();

        /**
         * WayfireShell - Wayfire Shell protocol implementation
         */
        WQt::Shell *wayfireShell();

        /**
         * InputInhibitManager - Wlr Input Inhibitor protocol implementation
         */
        WQt::InputInhibitManager *inputInhibitManager();

        /**
         * WindowManager - Wlr TopLevel Manager protocol implementation
         */
        WQt::WindowManager *windowManager();

        /**
         * ScreenCopyManager - ScreenCopy Manager protocol implementation
         */
        WQt::ScreenCopyManager *screenCopier();

        /**
         * DataControlManager - DataControl protocol implementation
         */
        WQt::DataControlManager *dataControlManager();

        /**
         * OutputPowerManager - Output Power Management protocol implementation
         */
        WQt::OutputPowerManager *outputPowerManager();

        /**
         * OutputManager - Output Management protocol implementation
         */
        WQt::OutputManager *outputManager();

        /**
         * SessionLock - Ext Session Lock protocol implementation
         */
        WQt::SessionLockManager *sessionLockManager();

        /**
         * Gamma Control - Gamma Control Manager
         */
        WQt::GammaControlManager *gammaControlManager();

        /**
         * Idle - KDE's idle protocol implementation
         */
        WQt::IdleManager *idleManager();

    private:
        /** Raw C pointer to this class */
        wl_registry *mObj = nullptr;

        /** wl_display object */
        wl_display *mWlDisplay = nullptr;

        /** wl_seat object */
        wl_seat *mWlSeat = nullptr;

        /** wl_shm object */
        wl_shm *mWlShm = nullptr;

        /** Connected outputs */
        QHash<uint32_t, WQt::Output *> mOutputs;

        /**
         * XdgShell objects;
         */
        xdg_wm_base *mXdgWmBase = nullptr;
        XdgShell *mXdgShell     = nullptr;

        /**
         * Layer Shell Objects
         */
        zwlr_layer_shell_v1 *mWlrLayerShell = nullptr;
        WQt::LayerShell *mLayerShell        = nullptr;

        /**
         * Wayfire Shell Objects
         */
        zwf_shell_manager_v2 *mWfShellMgr = nullptr;
        WQt::Shell *mWayfireShell         = nullptr;

        /**
         * Wayfire Shell Objects
         */
        zwlr_input_inhibit_manager_v1 *mWlrInhibitMgr = nullptr;
        WQt::InputInhibitManager *mInhibitManager     = nullptr;

        /**
         * TopLevel Objects
         */
        zwlr_foreign_toplevel_manager_v1 *mWlrWindowMgr = nullptr;
        WQt::WindowManager *mWindowMgr = nullptr;

        /**
         * ScreenCopyManager Objects
         */
        zwlr_screencopy_manager_v1 *mWlrScreenCopyMgr = nullptr;
        WQt::ScreenCopyManager *mScreenCopyMgr        = nullptr;

        /**
         * DataControlManager Objects
         */
        zwlr_data_control_manager_v1 *mWlrDataControlMgr = nullptr;
        WQt::DataControlManager *mDataControlMgr         = nullptr;

        /**
         * Output Power Manager Objects
         */
        zwlr_output_power_manager_v1 *mWlrOutputPowerMgr = nullptr;
        WQt::OutputPowerManager *mOutputPowerMgr         = nullptr;

        /**
         * Output Manager Objects
         */
        zwlr_output_manager_v1 *mWlrOutputMgr = nullptr;
        WQt::OutputManager *mOutputMgr        = nullptr;

        /**
         * Idle Objects
         */
        org_kde_kwin_idle *mKdeIdle = nullptr;
        WQt::IdleManager *mIdleMgr  = nullptr;

        /**
         * Gamma Control Objects
         */
        zwlr_gamma_control_manager_v1 *mWlrGammaCtrl = nullptr;
        WQt::GammaControlManager *mGammaCtrl         = nullptr;

        /**
         * Session Lock Objects
         */
        ext_session_lock_manager_v1 *mExtSessLockMgr = nullptr;
        WQt::SessionLockManager *mSessLockMgr        = nullptr;

        static const wl_registry_listener mRegListener;
        static void globalAnnounce( void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version );
        static void globalRemove( void *data, wl_registry *registry, uint32_t name );

        void handleAnnounce( uint32_t name, const char *interface, uint32_t version );
        void handleRemove( uint32_t name );

    signals:
        void errorOccured( ErrorType et );

        void outputAdded( WQt::Output * );
        void outputRemoved( WQt::Output * );
};
