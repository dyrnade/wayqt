/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QMargins>
#include <QSize>

struct zwlr_input_inhibit_manager_v1;
struct zwlr_input_inhibitor_v1;

namespace WQt {
    class InputInhibitManager;
    class InputInhibitor;
}

class WQt::InputInhibitManager : public QObject {
    Q_OBJECT;

    public:
        InputInhibitManager( zwlr_input_inhibit_manager_v1 *iimgr );
        ~InputInhibitManager();

        WQt::InputInhibitor *getInputInhibitor();

        zwlr_input_inhibit_manager_v1 *get();

    private:
        zwlr_input_inhibit_manager_v1 *mObj;
};

class WQt::InputInhibitor : public QObject {
    Q_OBJECT;

    public:
        InputInhibitor( zwlr_input_inhibitor_v1 *inhibitor );
        ~InputInhibitor();

        bool isValid();

        zwlr_input_inhibitor_v1 *get();

    private:
        zwlr_input_inhibitor_v1 *mObj;
};
